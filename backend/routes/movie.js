const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()
//POST --> ADD Movie data into Containerized MySQL table
router.post('/', (request, response) => {
  const {movie_title, movie_release_date,movie_time,director_name } = request.body

  const query = `
    INSERT INTO Movie
      (movie_title, movie_release_date,movie_time,director_name)
    VALUES
      ('${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
//movie_id, movie_title, movie_release_date,movie_time,director_name
//Display Movie using name from Containerized MySQL
router.get('/:movie_title', (request, response) => {
  const { movie_title } = request.params
  const query = `
    SELECT * 
    FROM Movie where movie_title = ${movie_title}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})
//UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table
router.put('/:movie_id', (request, response) => {
  const { movie_id } = request.params
  const { movie_release_date, movie_time } = request.body

  const query = `
    UPDATE Movie
    SET
    movie_release_date = '${movie_release_date}', 
    movie_time = ${movie_time}, 
      
    WHERE
    movie_id = ${movie_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

router.delete('/:movie_id', (request, response) => {
  const { movie_id } = request.params

  const query = `
    DELETE FROM Movie
    WHERE
    movie_id = ${movie_id}
  `
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result))
  })
})

module.exports = router
