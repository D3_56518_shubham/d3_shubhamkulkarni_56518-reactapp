CREATE TABLE Movie 
    (
     movie_id INTEGER PRIMARY KEY auto_increment,
     movie_title VARCHAR(100),
     movie_release_date DATE,
     movie_time TIME,
     director_name VARCHAR(100)
    );
 